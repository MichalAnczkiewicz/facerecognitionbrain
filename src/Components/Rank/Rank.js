import React from 'react';

const Rank = () => {
	return (
		<div> 
			<div className = 'white f3'>
				{'Michal, your current rank is...'}
			</div>
			<div className = 'white f1'>
				{'#5'}
			</div>
		</div>
	);
}

export default Rank;